const Usuario = require('../models').Usuario;
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
var sequelize= require('../models').sequelize;
var jwt = require('jsonwebtoken');

module.exports = {

	list(req,res) {

		return Usuario.findAll()
		.then(
			(usuarios) => res.status(200).send(usuarios))
		.catch(
			(error) => res.status(500).send(error)
			)
	},

	registro(req, res) {

		var hashedPassword = bcrypt.hashSync(req.body.password, 8);
		return Usuario.create({
			id: req.body.id,
			nombre : req.body.nombre,
			password : hashedPassword
		})
		.then(
			(usuario) => res.status(201).end())

		.catch(
			(error) => res.status(400).send(error))
	},
	changePass(req,res){
		var token = req.headers['x-access-token'];
		var decoded = jwt.decode(token);

		return Usuario.findByPk(decoded.id)
		.then(usuario=> {
			var esPasswordActual = bcrypt.compareSync(req.body.password, usuario.password);
			if(esPasswordActual){

				if(req.body.nuevoPassword==req.body.nuevoPassword2){
					var hashedPassword = bcrypt.hashSync(req.body.nuevoPassword, 8);
					return Usuario.update(
						{password:hashedPassword},
						{
							where:{
								id:usuario.id
							}
						}
						).then(usuario=> res.status(200).end())
					.catch(error=> {
						console.log(error);
						res.status(500).send(error);
					})
				}else{
					res.status(400).send({error:"Los passwords no coinciden"})
				}
			}
			else{
				res.status(400).send({error: "Password actual no coincide"})
			}
		})
		.catch(error=> {
			console.log(error);
			res.status(500).send(error)
		})
	},
	login(req, res) {
		return Usuario.findAll({
			where: {
				nombre: req.body.nombre
			}
		})
		.then(usuario => {

			if (!usuario[0]) return res.status(404).send('Usuario no existe.');
			var passwordIsValid = bcrypt.compareSync(req.body.password, usuario[0].password);
			if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
			
			var token = jwt.sign({ id: usuario[0].id }, config.secret, {
      								expiresIn: 86400 // expira en 24 horas
      							});
			res.status(200).send({ auth: true, token: token })
		})
		.catch(
			(error) => res.status(400).send(error))
	},
	logout(req, res) {
		res.status(200).send({ auth: false, token: null });
	},
	delete (req, res) {
		return Usuario.destroy({
			where: {
				id:req.params.id
			}
		})
		.then(usuario => {
			res.status(200).end();
		})
		.catch(
			(error) => res.status(400).send(error))
		

	}


}
