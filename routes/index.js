var express = require('express');
var router = express.Router();
const VerifyToken = require('../auth/VerifyToken');

const usuarioController = require('../controllers').usuarioController;


//registro de usuario
router.post('/usuario', usuarioController.registro);
//cambio de password
router.put('/usuario/resetPassword', VerifyToken,usuarioController.changePass);
//baja de usuario
router.delete('/usuario/:id', VerifyToken, usuarioController.delete);


//login
router.post('/login',usuarioController.login);

//logout
router.get('/logout', usuarioController.logout);
//home page

router.get('/',VerifyToken,usuarioController.list);


module.exports = router;
