
var chai = require('chai');
var chaiHttp = require('chai-http');
var assert = require('chai').assert;

chai.use(chaiHttp);

var server = require('../app');

before(done => {
    console.log('\n\n-----------------------\n--\n-- START TEST\n--\n-------------------------');
    done();
});
after(done => {
    console.log('\n\n-----------------------\n--\n-- END TEST\n--\n-------------------------');
    done();
});


describe('#Registro de usuario', () => {
    it('post registro', done => {
        chai.request(server)
        .post('/usuario')
        .send({id: '30',nombre:'prueba',password:'prueba'})
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(201);
            done();

        });
    }).timeout(0);
});
var token;

describe('#Login de usuario registrado', () => {
    it('login usuario prueba', done => {
        chai.request(server)
        .post('/login')
        .send({nombre:'prueba',password:'prueba'})
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(200);
            chai.expect(res.body.auth).to.equal(true);
            token=res.body.token;
            done();

        });
    }).timeout(0);
});

describe('#Acceder a ruta principal', () => {
    it('get lista de usuarios', done => {
        chai.request(server)
        .get('/')
        .set('x-access-token', token)
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(200);
            done();
            
        });
    }).timeout(0);
});
describe('#Intentar acceder sin token a ruta principal', () => {
    it('get lista de usuarios, sin token', done => {
        chai.request(server)
        .get('/')
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(401);
            done();
            
        });
    }).timeout(0);
});
describe('#Intentar acceder con token no valido a ruta principal', () => {
    it('get lista de usuarios, token no valido', done => {
        chai.request(server)
        .get('/')
        .set('x-access-token', "tokenNoValido")
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(401);
            done();
            
        });
    }).timeout(0);
});

describe('#Intentar cambiar password con token no valido', () => {
    it('put usuario, token no valido', done => {
        chai.request(server)
            .put('/usuario/resetPassword')
            .set('x-access-token', "tokenNoValido")
            .send({password:'prueba',nuevoPassword:'prueba2',nuevoPassword2:'prueba2'})
            .end(function (err, res) {
                chai.expect(res.statusCode).to.equal(401);
                done();

            });
    }).timeout(0);
});
describe('#Intentar cambiar password de usuario, sin token', () => {
    it('put usuario, sin token', done => {
        chai.request(server)
            .put('/usuario/resetPassword')
            .send({password:'prueba',nuevoPassword:'prueba2',nuevoPassword2:'prueba2'})
            .end(function (err, res) {
                chai.expect(res.statusCode).to.equal(401);
                done();
            });
    }).timeout(0);
});
describe('#Intentar Cambio de password, password no coincide', () => {
    it('cambio de password, password no valido', done => {
        chai.request(server)
            .put('/usuario/resetPassword')
            .set('x-access-token', token)
            .send({password:'password',nuevoPassword:'prueba2',nuevoPassword2:'prueba2'})
            .end(function (err, res) {
                chai.expect(res.body.error).to.equal("Password actual no coincide");
                done();

            });
    }).timeout(0);
});
describe('#Intentar Cambio de password, reingreso de password nuevo no coincide', () => {
    it('cambio de password, reingreso de password nuevo no valido', done => {
        chai.request(server)
            .put('/usuario/resetPassword')
            .set('x-access-token', token)
            .send({password:'prueba',nuevoPassword:'prueba2',nuevoPassword2:'prueba3'})
            .end(function (err, res) {
                chai.expect(res.body.error).to.equal("Los passwords no coinciden");
                done();

            });
    }).timeout(0);
});
describe('#Cambio de password', () => {
    it('cambio de password, de prueba a prueba2', done => {
        chai.request(server)
            .put('/usuario/resetPassword')
            .set('x-access-token', token)
            .send({password:'prueba',nuevoPassword:'prueba2',nuevoPassword2:'prueba2'})
            .end(function (err, res) {
                chai.expect(res.statusCode).to.equal(200);
                done();

            });
    }).timeout(0);
});
describe('#Login de usuario con nuevo password', () => {
    it('login usuario prueba con nuevo password', done => {
        chai.request(server)
            .post('/login')
            .send({nombre:'prueba',password:'prueba2'})
            .end(function (err, res) {
                chai.expect(res.statusCode).to.equal(200);
                chai.expect(res.body.auth).to.equal(true);
                token=res.body.token;
                done();

            });
    }).timeout(0);
});
describe('#Intentar eliminar usuario registrado, sin token', () => {
    it('delete usuario prueba, sin token', done => {
        chai.request(server)
        .delete('/usuario/30')
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(401);
            done();

        });
    }).timeout(0);
});
describe('#Intentar eliminar usuario, con token no valido', () => {
    it('delete usuario prueba, token no valido', done => {
        chai.request(server)
        .delete('/usuario/30')
        .set('x-access-token', "tokenNoValido")
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(401);
            done();
            
        });
    }).timeout(0);
});

describe('#Eliminacion de usuario registrado', () => {
    it('delete usuario prueba', done => {
        chai.request(server)
        .delete('/usuario/30')
        .set('x-access-token', token)
        .end(function (err, res) {
            chai.expect(res.statusCode).to.equal(200);
            done();

        });
    }).timeout(0);
});

describe('#Logout', () => {
    it('get logout', done => {
        chai.request(server)
        .get('/logout')
        .end(function (err, res) {
            token=res.body.token;
            chai.expect(res.statusCode).to.equal(200);
            done();
            
        });
    }).timeout(0);
});