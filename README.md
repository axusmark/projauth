# projAuth
Sistema API de autenticación de usuarios utilizando JSON Web Token.

## Integración continua con Gitlab CI
Para inicializar un entorno CI en Gitlab, se creó un archivo ```.gitlab-ci.yml``` en la raíz del repositorio.   
Este archivo contiene una descripción paso a paso sobre cómo se construirá el proyecto. GitLab CI busca este archivo en particular dentro del repositorio para determinar cómo debe probar el código.
El archivo creado para este caso es el siguiente:
```yaml
image: node:latest

stages:
  - build
  - test
  - deploy

cache:
  paths:
    - node_modules/


build:
  stage: build
  script:
    - npm install
  artifacts:
    paths:
      - node_modules/

test:
  stage: test
  script: 
    - npm test
  
deploy:
  stage: deploy
  image: ruby:latest
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=proj-auth-prod --api-key=a76badb9-1e29-4dfa-9cbc-c6e9b0086ea7
  only:
    - tags

```
El archivo de configuración comienza declarando una imagen de docker, especificando la última versión de NodeJS que será utilizado en el build.
```yaml
image: node:latest
```
A continuación, se define los diferentes procesos de integración continua que se ejecutará (stages).
```yaml
stages:
  - build
  - test
  - deploy
```

Luego de definir los stages, la configuración incluye una caché que especifica el directorio en donde serán instaladas las dependencias que necesita el proyecto. Esta caché será utilizada en los siguientes stages.
```yaml
cache:
  paths:
    - node_modules/
```

El stage Build se encarga de instalar las dependencias del proyecto en el directorio ```node_modules```
```yaml
build:
  stage: build
  script:
    - npm install
  artifacts:
    paths:
      - node_modules/
```

El stage Test declara el comando que ejecutará las pruebas unitarias, luego de acceder a lo que se produce en el stage Build, que son las dependencias del proyecto.

```yaml
test:
  stage: test
  script: 
    - npm test
 
```
El script ```npm test ``` ejecuta el comando especificado en ```package.json ```:
 ```json
 {"test": "cross-env NODE_ENV=test mocha ./test/autenticacion.js"}
```
Por último, el stage Deploy instala dpl para realizar deploy a Heroku. 
Las credenciales que se provee a Heroku son el nombre de la app (--app) y la API Key de la cuenta de Heroku (--api-key).
Este stage sólo se ejecuta al realizar un tag en el repositorio.

OBS: en commits anteriores, este stage estaba configurado para ejecutarse tambien al realizar push al master, para comprobar cómo funcionaba el stage.
```yaml
deploy:
  stage: deploy
  image: ruby:latest
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=proj-auth-prod --api-key=a76badb9-1e29-4dfa-9cbc-c6e9b0086ea7
  only:
    - tags
```
### APP en Heroku
```proj-auth``` - [https://proj-auth-prod.herokuapp.com/](https://proj-auth-prod.herokuapp.com/)
## Instalación

El sistema  requiere [Node.js](https://nodejs.org/) para ejecutarse.

Clonar el proyecto con git.
Instale las dependencias de desarrollo y producción e inicie el servidor.

```sh
$ git clone https://gitlab.com/axusmark/projauth.git
$ cd proj-auth
$ npm install -d
$ npm start
```

Para entornos de producción...

```sh
$ npm install --production
$ NODE_ENV=production node bin/server.js
```
## Uso de la API
### * Usuario por defecto
usuario: admin

password: admin

### * Login
Comprueba los credenciales del usuario y retorna un token para inicio de sesión.
##### HTTP Request
`POST /login`

```json 
{
    "nombre":"[nombre_usuario]",
    "password":"[password]"
}
```
Retorna un objeto JSON con el siguiente formato:
```json 
{
    "auth": true,
    "token": "[JSON Web Token]"
}
```
En caso de error, retorna:
```json
{
    "auth": false,
    "token": null
}
```
### * logout
Reemplaza el token alojado en la caché de la aplicación con un token vacío.
##### HTTP Request
`GET /logout`
Retorna un objeto JSON con el siguiente formato:
```json
{
    "auth": false,
    "token": null
}
```
### * Acceso a ruta principal del sitio
Acceso a Home Page de prueba.
##### HTTP Request
`GET /`
##### Cabecera HTTP:

```json
{
    "x-access-token": "[JSON Web Token obtenido en el login]"
}
``` 
Retorna:
```json
[
    {
        "id": 1,
        "nombre": "usuario1",
        "password": "password1"
    },
    {
        "id": 2,
        "nombre": "usuario2",
        "password": "password2"
    },
    {
        "id": "n",
        "nombre": "usuarioN",
        "password": "passwordN"
    }
]
```
En caso de error, puede retornar los siguientes tipos de respuesta:
* No se envió el token en la cabecera HTTP.
```json

{
    "auth": false,
    "message": "No se proveyo el token"
}
```
* El token enviado en la cabecera HTTP no es válido.
```json

{
    "auth": false,
    "message": "Fallo al autenticar el token"
}
```
### * Registro
Alta de usuarios para realizar test de usuarios de prueba.
##### HTTP Request
`POST /usuario`
```json 
{
    "id":"[id]",
    "nombre":"[nombre_usuario]",
    "password":"[password]"
}
```
Retorna
```
HTTP status 201 (created) 
```
En caso de error, retorna:
```
HTTP status 400 (Bad Request)
```
### * Cambio de contraseña
Cambiar contraseña del usuario
##### HTTP Request
`PUT /usuario/resetPassword`
##### Cabecera HTTP:

```json
{
    "x-access-token": "[JSON Web Token obtenido en el login]"
}
```
##### Body:
```json
{
    "password":"[password actual]",
    "nuevoPassword":"[nuevo password]", 
    "nuevoPassword2":"[repetir nuevo password]"
    
}
```
Retorna:
```
HTTP status 200 (OK) 
```
En caso de error, puede retornar los siguientes tipos de respuesta:
* No se envió el token en la cabecera HTTP.
```json

{
    "auth": false,
    "message": "No se proveyo el token"
}
```
* El token enviado en la cabecera HTTP no es válido.
```json

{
    "auth": false,
    "message": "Fallo al autenticar el token"
}
```
* Contraseña actual no coincide
```json

{
    "error": "Password actual no coincide"
}
```
* Comprobación de contraseña nueva no coincide
```json

{
    "error": "Los passwords no coinciden"
}
```
### * Eliminación de usuario
Baja de usuarios para realizar test de usuarios de prueba.
##### HTTP Request
`GET /delete/[id]`
 [id]: ID del usuario a eliminar.
##### Cabecera HTTP:

```json
{
    "x-access-token": "[JSON Web Token obtenido en el login]"
}
``` 
Retorna:
```
HTTP status 200 (OK) 
```
En caso de error, puede retornar los siguientes tipos de respuesta:
* No se envió el token en la cabecera HTTP.
```json

{
    "auth": false,
    "message": "No se proveyo el token"
}
```
* El token enviado en la cabecera HTTP no es válido.
```json

{
    "auth": false,
    "message": "Fallo al autenticar el token"
}
```


