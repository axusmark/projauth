'use strict';
module.exports = (sequelize, DataTypes) => {
	const Usuario = sequelize.define('Usuario', {
		id: {type:DataTypes.INTEGER,primaryKey: true, field:'id'},
		nombre: {type:DataTypes.STRING,allowNull:false},
		password: {type:DataTypes.STRING,allowNull:false}
	}, {tableName:'usuarios',timestamps:false});
	Usuario.associate = function(models) {
    // associations can be defined here
};
return Usuario;
};
